# Flutter Animations

Flutter project to learn the basic Flutter animations principles for students of the Multimedia Applications Programming course

## Application

:rocket: See it in action [here](https://winmaster-flutter-portfolio.gitlab.io/flutter-animations/)
 on Gitlab page.


## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.


## Helpful links and materials about Animations

- [Introduction (theoretical)](https://docs.flutter.dev/development/ui/animations)
- [Animations tutorial](https://docs.flutter.dev/development/ui/animations/tutorial)
- [Staggered animations](https://docs.flutter.dev/development/ui/animations/staggered-animations)

The best tutorials in my opinion:

- [Basics of Flutter animations](https://flexiple.com/app/basics-of-flutter-animations/)
- [Advanced Flutter animations](https://flexiple.com/app/advanced-flutter-animations/)