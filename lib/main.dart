import 'package:flutter/material.dart';
import 'staggered_animation.dart';
import 'tween_animation.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Animations Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatelessWidget {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Menu'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            SizedBox(
              width: 300,
              height: 100,
              child: ElevatedButton(
                child: const Text('Tween animations'),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => TweenAnimationExample()),
                  );
                },
              ),
            ),
            SizedBox(
              width: 300,
              height: 100,
              child: ElevatedButton(
                child: const Text('Staggered animations'),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => StaggeredAnimationExample()),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
