import 'package:flutter/material.dart';
import 'easter_egg.dart';

class StaggeredAnimationExample extends StatefulWidget {
  @override
  _StaggeredAnimationState createState() => _StaggeredAnimationState();
}

class _StaggeredAnimationState extends State<StaggeredAnimationExample> with SingleTickerProviderStateMixin {
  AnimationController _controller;

  Animation _androidIconAnimation;
  Animation _contentAnimation;
  Animation _rotationAnimation;
  Animation _listAnimation;
  Animation _fabAnimation;
  @override
  void initState() {
    super.initState();
    _controller =
        AnimationController(vsync: this, duration: Duration(seconds: 4));
    _androidIconAnimation = Tween(begin: 0.0, end: 200.0).animate(
        CurvedAnimation(
            parent: _controller,
            curve: Interval(0.0, 0.20, curve: Curves.easeOut)));
    _contentAnimation = Tween(begin: 0.0, end: 75.0).animate(CurvedAnimation(
        parent: _controller,
        curve: Interval(0.0, 0.40, curve: Curves.easeOut)));
    _rotationAnimation = Tween(begin: 0.0, end: 3.14).animate(_controller);
    _listAnimation = Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: _controller,
        curve: Interval(0.40, 0.75, curve: Curves.easeOut)));
    _fabAnimation = Tween(begin: 0.0, end: 5.0).animate(CurvedAnimation(
        parent: _controller,
        curve: Interval(0.75, 1.0, curve: Curves.easeInCubic)));
    _controller.forward();
    _controller.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: _buildAppBar(),
      body: _buildBody(),
      floatingActionButton: _buildFAB(),
    );
  }

  Widget _buildAppBar() {
    return AppBar(
      brightness: Brightness.light,
      backgroundColor: Colors.blue,
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.android_outlined),
          color: Colors.lightGreenAccent,
          onPressed: () {},
          iconSize: _androidIconAnimation.value,
        ),
      ],
      elevation: 10.0,
    );
  }

  Widget _buildBody() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Expanded(
          child: Container(
            padding: EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 16.0,
                ),
                Text(
                  "Good Afternoon",
                  style: TextStyle(fontSize: _contentAnimation.value, fontWeight: FontWeight.w600),
                ),
                SizedBox(
                  height: 18.0,
                ),
                Transform.rotate(
                  angle: _rotationAnimation.value,
                  child: Text(
                    "Here are your animations for today",
                    style: TextStyle(fontSize: 18.0),
                  ),
                ),
              ],
            ),
          ),
        ),
        Expanded(
          flex: 2,
          child: Opacity(
            opacity: _listAnimation.value,
            child: ListView.separated(
              itemCount: 3,
              separatorBuilder: (context, index) => SizedBox(height: 16),
              itemBuilder: (context, index) {
                return SizedBox(
                  width: 100,
                  height: 100,
                  child: EasterEgg(),
                );
              },
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildFAB() {
    return Transform.scale(
      scale: _fabAnimation.value,
      child: FloatingActionButton(
        foregroundColor: Colors.red,
        backgroundColor: Colors.orangeAccent,
        onPressed: () {},
        child: Icon(Icons.add),
      ),
    );
  }
}